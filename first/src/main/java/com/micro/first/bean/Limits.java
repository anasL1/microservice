package com.micro.first.bean;

public class Limits {

	private int minimum;
	private int maximum;

	public Limits(int min, int max) {
		this.maximum = max;
		this.minimum = min;
	}

	public int getMinimum() {
		return minimum;
	}

	public void setMinimum(int minimum) {
		this.minimum = minimum;
	}

	public int getMaximum() {
		return maximum;
	}

	public void setMaximum(int maximum) {
		this.maximum = maximum;
	}

}
