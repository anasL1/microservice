package com.micro.first.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.micro.first.bean.Limits;
import com.micro.first.configuration.Configuration;

@RestController
public class LimitController {
	
	@Autowired
	Configuration configuration;

	@GetMapping("limits")
	public Limits retrieveLimit() {
		return new Limits(configuration.getMin(),configuration.getMax());
	}
}
