package com.micro.currencyexchangesevice;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CurrencyExchangeController {

	@Autowired
	Environment environment;
	
	@Autowired
	CurrencyExchangeRepo currencyExchangeRepo;

	@GetMapping("/currency-exchange/from/{from}/to/{to}")
	public CurrencyExchange currencyExchange(@PathVariable String from,@PathVariable String to)
	{
		CurrencyExchange currencyExchange = currencyExchangeRepo.findByFromAndTo(from, to);
		currencyExchange.setEnviornment(environment.getProperty("local.server.port"));
		
		return currencyExchange;
	}

}
