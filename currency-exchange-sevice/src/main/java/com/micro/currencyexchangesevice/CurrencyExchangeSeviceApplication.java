package com.micro.currencyexchangesevice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CurrencyExchangeSeviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CurrencyExchangeSeviceApplication.class, args);
	}

}
