package com.micro.currencyexchangesevice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import io.github.resilience4j.retry.annotation.Retry;

@RestController
public class CircuitBreakerController {
	Logger logger = LoggerFactory.getLogger(CircuitBreakerController.class);

	@GetMapping("/breaker-api")
//	@Retry(name="sample-api",fallbackMethod = "hardCodedResponse")
//	@CircuitBreaker(name="sample-api",fallbackMethod = "hardCodedResponse")
	@RateLimiter(name="sample-api")
	public String sampleAPI() {
		logger.info("Sample-Api called");
		ResponseEntity<String> responseEntity = new RestTemplate().getForEntity("http://localhost:8080/random", String.class);
		return responseEntity.getBody();
	}
	public String hardCodedResponse(Exception ex) {
		return "fallbackResponse"; 
	}
}
