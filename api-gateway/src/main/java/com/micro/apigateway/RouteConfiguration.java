package com.micro.apigateway;

import java.util.function.Function;

import org.springframework.cloud.gateway.route.Route;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.Buildable;
import org.springframework.cloud.gateway.route.builder.GatewayFilterSpec;
import org.springframework.cloud.gateway.route.builder.PredicateSpec;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.cloud.gateway.route.builder.UriSpec;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RouteConfiguration {

	@Bean
	RouteLocator gatewayRouter(RouteLocatorBuilder routeLocatorBuilder) {
		Function<GatewayFilterSpec, UriSpec> fn = f -> f.addRequestHeader("MYHEADER", "MYURI");
		Function<PredicateSpec, Buildable<Route>> routeFunction = p -> p.path("/get").filters(fn)
				.uri("http://httpbin.org:80");
		Function<PredicateSpec, Buildable<Route>> currencyExchangeroute = p -> p.path("/currency-exchange/**")
				.uri("lb://currency-exchange");
		Function<PredicateSpec, Buildable<Route>> currencyConversionRoute = p -> p.path("/currency-conversion/**")
				.uri("lb://currency-conversion");
		Function<PredicateSpec, Buildable<Route>> currencyConversionRouteFeign = p -> p.path("/currency-conversion-feign/**")
				.uri("lb://currency-conversion");
		Function<PredicateSpec, Buildable<Route>> currencyConversionRouteNew = p -> p
				.path("/currency-conversion-new/**")
				.filters(f -> f.rewritePath("/currency-conversion-new/(?<segment>.*)", "/currency-conversion-feign/${segment}"))
				.uri("lb://currency-conversion");
		return routeLocatorBuilder.routes().route(routeFunction).route(currencyExchangeroute).route(currencyConversionRouteFeign)
				.route(currencyConversionRoute).route(currencyConversionRouteNew).build();
	}
}
