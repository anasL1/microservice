package com.micro.currencyconversionsevice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class CurrencyConversionSeviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CurrencyConversionSeviceApplication.class, args);
	}

}
