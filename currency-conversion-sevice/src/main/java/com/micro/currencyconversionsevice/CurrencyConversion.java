package com.micro.currencyconversionsevice;

import java.math.BigDecimal;


public class CurrencyConversion {

	private Long id;
	private String from;
	private String to;
	private BigDecimal conversionMultiple;
	private BigDecimal totalClaculatedAmount;
	private BigDecimal quantity;
	private String enviornment;
	
	
	public CurrencyConversion() {
		super();
	}
	public CurrencyConversion(Long id, String from, String to, BigDecimal conversionMultiple,
			BigDecimal totalClaculatedAmount, BigDecimal quantity, String enviornment) {
		super();
		this.id = id;
		this.from = from;
		this.to = to;
		this.conversionMultiple = conversionMultiple;
		this.totalClaculatedAmount = totalClaculatedAmount;
		this.quantity = quantity;
		this.enviornment = enviornment;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public BigDecimal getConversionMultiple() {
		return conversionMultiple;
	}
	public void setConversionMultiple(BigDecimal conversionMultiple) {
		this.conversionMultiple = conversionMultiple;
	}
	public BigDecimal getTotalClaculatedAmount() {
		return totalClaculatedAmount;
	}
	public void setTotalClaculatedAmount(BigDecimal totalClaculatedAmount) {
		this.totalClaculatedAmount = totalClaculatedAmount;
	}
	public BigDecimal getQuantity() {
		return quantity;
	}
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}
	public String getEnviornment() {
		return enviornment;
	}
	public void setEnviornment(String enviornment) {
		this.enviornment = enviornment;
	}
	
	

}
